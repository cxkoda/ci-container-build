## Gitlab-Runner
I had to set up an own gitlab-runner since the university runner is not allowed to fetch from the ubuntu repo.


* `apt install gitlab-runner`
* Register runner: `sudo gitlab-runner register -n --url https://gitlab.example.com/ --registration-token your-token  --executor docker --description "docker-builder" --docker-image "docker:latest" --docker-privileged`
* add docker volume in `/etc/gitlab-runner/config.toml`. This should look like: 
  ```
  [[runners]]
  name = "docker-builder"
  url = "https://git.uibk.ac.at/"
  token = "..."
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
  [runners.docker]
    tls_verify = false
    image = "docker:latest"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    shm_size = 0
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
  ```
* start runner: `sudo gitlab-runner run`